package com.devcamp.animalapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainAnimal {
    @CrossOrigin
    @GetMapping("/cats")
    public ArrayList<Cat> catsList() {
        Cat cat1 = new Cat("Cash");
        Cat cat2 = new Cat("Moon");
        Cat cat3 = new Cat("Viois");

        ArrayList<Cat> cats = new ArrayList<>();
        cats.add(cat3);
        cats.add(cat2);
        cats.add(cat1);
        return cats;

    }

    @GetMapping("/dogs")
    public ArrayList<Dog> dogsList() {
        Dog dog1 = new Dog("Bee");
        Dog dog2 = new Dog("bun");
        Dog dog3 = new Dog("Ant");

        ArrayList<Dog> dogs = new ArrayList<>();
        dogs.add(dog1);
        dogs.add(dog2);
        dogs.add(dog3);
        return dogs;

    }

}
